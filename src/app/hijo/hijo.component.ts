import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-hijo',
  // templateUrl: './hijo.component.html',
  styleUrls: ['./hijo.component.css'],
  template: `
  <strong>Este es el {{title}}</strong>
  <ul>
  <li>{{propiedad_uno}}</li>
  <li>{{propiedad_dos}}</li>
  </ul>

  <button (click)="enviar()">Enviar datos al padre</button>
  `
})
export class HijoComponent implements OnInit {
  public title: string;

  @Input('texto1') propiedad_uno: string;
  @Input('texto2') propiedad_dos: string;

  @Output() desde_el_hijo = new EventEmitter();

  constructor() {
    this.title = 'Componente hijo';
  }

  ngOnInit() {
    console.log(this.propiedad_uno);
    console.log(this.propiedad_dos);
    this.enviar();
  }

  enviar() {
    this.desde_el_hijo.emit({
      nombre: 'Víctor Robles WEB',
      web: 'victorroblesweb.es',
      twitter: '@victorobs'
    });
  }

}
