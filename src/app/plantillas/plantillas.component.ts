import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-plantillas',
  templateUrl: './plantillas.component.html',
  styleUrls: ['./plantillas.component.css']
})
export class PlantillasComponent implements OnInit {
  public titulo;
  public administrador;

  public dato_externo = 'Víctor Robles';
  public identity = {
    id: 1,
    web: 'victorroblesweb.es',
    tematica: 'Desarrollo web'
  };

  public datos_del_hijo;

  constructor() {
    this.titulo = 'Plantillas ngTemplate en Angular';
    this.administrador = true;
  }

  ngOnInit() {
  }

  cambiar(value) {
    this.administrador = value;
  }

  recibirDatos(event) {
    console.log(event);
    this.datos_del_hijo = event;
  }

}
